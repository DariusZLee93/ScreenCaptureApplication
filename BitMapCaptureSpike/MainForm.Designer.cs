﻿using BitMapCaptureSpike.Controls;

namespace BitMapCaptureSpike
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private BackgroundControl userControl11;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userControl11 = new BitMapCaptureSpike.Controls.BackgroundControl();
            this.SuspendLayout();
            // 
            // userControl11
            // 
            this.userControl11.Name = "userControl11";
            this.userControl11.TabIndex = 0;
            
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 500);
            this.Controls.Add(this.userControl11);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.BackColor = System.Drawing.Color.Beige;

            this.ResumeLayout(false);

        }


        #endregion

    }
}

