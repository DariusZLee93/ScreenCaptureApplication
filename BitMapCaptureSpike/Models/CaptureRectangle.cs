﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitMapCaptureSpike.Models
{
    public class CaptureRectangle
    {
        
        private readonly Rectangle captureRectangle;

        public CaptureRectangle(Point StartCorner, Point EndCorner)
        {
            if(StartCorner.X >= EndCorner.X || StartCorner.Y >= EndCorner.Y)
            {
                throw new ArgumentException("Start corner must always be closer to (0, 0) then "
                    + "the second point");
            }

            var size = new Size(EndCorner.X - StartCorner.X, EndCorner.Y - StartCorner.Y);

            this.captureRectangle = new Rectangle(StartCorner, size);
        }

        public Rectangle GetRectangle()
        {
            return this.captureRectangle;
        }

        public int Width()
        {
            return this.captureRectangle.Width;
        }

        public int Height()
        {
            return this.captureRectangle.Height;
        }
    }
}
