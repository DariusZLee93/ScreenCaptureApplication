﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitMapCaptureSpike.Models
{
    public class CaptureBitmap
    {
        public readonly CaptureRectangle Rectangle;
        public readonly Bitmap BitMap;
            
        public CaptureBitmap(Bitmap BitMap, CaptureRectangle Rectangle)
        {
            this.BitMap = BitMap;
            this.Rectangle = Rectangle;
        }
    }
}
