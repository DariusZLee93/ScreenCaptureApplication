﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BitMapCaptureSpike.Controls
{
    public partial class ClickCaptureControl : UserControl
    {
        public ClickCaptureControl()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            InitializeComponent();
        }
    }
}
