﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace BitMapCaptureSpike.Controls
{
    partial class BackgroundControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private ClickCaptureControl clickCapture;

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clickCapture = new ClickCaptureControl();

            components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Size = new System.Drawing.Size(600, 400);
            this.Location = new Point(0, 0);
            this.BackgroundImage = Image.FromFile(
                Path.Combine(Directory.GetCurrentDirectory(), "Resources", "sampleImage.jpg"));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;

            this.Controls.Add(clickCapture);
        }

        #endregion
    }
}
