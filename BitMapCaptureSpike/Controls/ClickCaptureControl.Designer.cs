﻿using BitMapCaptureSpike.Models;
using BitMapCaptureSpike.Services;
using BitMapCaptureSpike.Services.Interfaces;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace BitMapCaptureSpike.Controls
{
    partial class ClickCaptureControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private Point LeftClickDownPoint = default(Point);

        private readonly IScreenCaptureService captureService = new ScreenCaptureService();

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && this.LeftClickDownPoint == default(Point))
            {
                this.LeftClickDownPoint = e.Location;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && this.LeftClickDownPoint != default(Point))
            {
                var rect = new CaptureRectangle(this.LeftClickDownPoint, e.Location);

                var ParentBitmap = new Bitmap(600, 400);
                var ExtractedBitmap = new Bitmap(rect.Width(), rect.Height());

                this.Parent.DrawToBitmap(ParentBitmap, new Rectangle(0, 0, 600, 400));
                ExtractedBitmap = ParentBitmap.Clone(rect.GetRectangle(), ParentBitmap.PixelFormat);

                //this.captureService.StoreCapture(new CaptureBitmap(Bitmap, rect));

                ExtractedBitmap.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "random.jpeg"), 
                    ImageFormat.Jpeg);

                this.LeftClickDownPoint = default(Point);
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
    
            components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Location = new System.Drawing.Point(0, 0);
            this.Size = new System.Drawing.Size(600, 400);

            this.BackColor = System.Drawing.Color.Transparent;
        }

        #endregion
    }
}
