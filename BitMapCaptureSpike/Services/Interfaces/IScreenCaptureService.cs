﻿using BitMapCaptureSpike.Models;

namespace BitMapCaptureSpike.Services.Interfaces
{
    public interface IScreenCaptureService
    {
        bool StoreCapture(CaptureBitmap capture);
        CaptureBitmap GetCapture();
    }
}
