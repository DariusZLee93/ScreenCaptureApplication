﻿using BitMapCaptureSpike.Models;
using BitMapCaptureSpike.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitMapCaptureSpike.Services
{
    public class ScreenCaptureService : IScreenCaptureService
    {
        private CaptureBitmap capture;

        public ScreenCaptureService()
        {
            capture = default(CaptureBitmap);
        }

        public bool StoreCapture(CaptureBitmap capture)
        {
            this.capture = capture;

            return true;
        }

        public CaptureBitmap GetCapture()
        {
            return this.capture;
        }
    }
}
